$('body').ready(function(){
   
    $.getJSON('config.json', _.bind(function(data){
 
    	App.views=new Array();
    	App.initialize(data);
    	 console.log('entra a app.js');
    }, this));
    
});

App = {	    
		
	initialize: function(config){
    
        this.optionsContainer = $('#opcionesContainer');
        this.currentView = null;
        this.templates = $('<div></div>');
        this.loadTemplates();
        this.config=config;
        this.url = config.server;
        this.context = config.context;
        
    },
    
   
            
    loadTemplates: function(){
        console.log('App.loadTemplates()');
        this.templates.load('templates.html', _.bind(function(){
            this.initApp();
        }, this));
    },
    
    initApp: function()
    {
    	try{

    		this.views ={
				listaView : new ListaView( {el: $('#listaView', this.templates)} ),
				crearView	: new CrearView( {el: $('#crearView', this.templates)} ),
				verView : new VerView( {el: $('#verView', this.templates)} ),
				editarView : new EditarView( {el: $('#editarView', this.templates)} )
			};
			
	        this.setView("start");
	        
    	}catch(err){
    		alert( "error en la configuracion del producto " + err );
    	}
    },
            
    setView: function(transition)
    {
    	var flag=false;
    	for(var i=0;i<this.config.transitions.length;i++)
    	{
    		var obj=null;
    		obj=this.config.transitions[i];
    		if(obj.id==transition){
    			console.log("transition "+transition);
        		//console.log("view "+JSON.stringify(obj));
        		var newView=this.views[obj.view];
    	        if(this.currentView != null){
    	            this.currentView.onRemove();
    	            $(this.currentView.el).detach();
    	        }
    	        container=$(obj.region);
    	        container.html(newView.el);
    	        newView.onRender();
    	        this.currentView = newView;
    	        flag=true;
    		}
    	}
    	if(flag==false){
    		alert("Transicion no v�lida");
    	}
    }
};

App.Delegate = App.Delegate || {};



