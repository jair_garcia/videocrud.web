// Mock of the delegate
//Por ejemplo http://localhost:8080/videocrud.services.subsystem/webresources/VideoService
var video_service_url =  'http://' + App.url + '/' + App.context + '/webresources/VideoService'; 
App.Delegate.VideoDelegate = App.Delegate.VideoDelegate || {
	
	video:function(evento, nombre, url,callback, callbackError){

		$.ajax({
		         url: 'http://' + App.url + '/' + App.context + '/webresources/VideoService', 
		         type: 'POST',
		         data: {'eventos': evento, 'name': nombre,'dir':url}
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	},

	edicion:function(id ,callback, callbackError){
	
		$.ajax({
		         url: 'http://' + App.url + '/' + App.context + '/webresources/VideoService/'+id, 
		         type: 'GET',
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	},
	
	eliminar:function(evento, nombre, url,callback, callbackError){
	
		$.ajax({
		         url: 'http://' + App.url + '/' + App.context + '/webresources/VideoService', 
		         type: 'SET',
		         data: {'eventos': evento, 'name': nombre,'dir':url}
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	}
	//Interfaz with server
	,
	insertVideoDTO:function(name, description, url,callback, callbackError){
		$.ajax({
		         url: video_service_url , 
		         type: 'POST',
		         data: {'name': name, 'description': description,'url':url}
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	},
	getVideoDTO:function(id,callback, callbackError){
		$.ajax({
		         url: video_service_url+"/"+id , 
		         type: 'GET'
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	},
	getListVideoDTOs:function(callback, callbackError){
		$.ajax({
	         url: video_service_url , 
	         type: 'GET'
	      }).done(_.bind(function(data)
		  {
	    	  console.log('Ok: ' + JSON.stringify(data));
	          callback(data);
	      }, this)).error(_.bind(function(data){
	          console.log('Error: ' + JSON.stringify(data));
	          callbackError(data);
	      }));
	},
	deleteVideoDTO:function(id,callback, callbackError){
		$.ajax({
			url: video_service_url+"/"+id , 
	         type: 'DELETE'
	      }).done(_.bind(function(data)
		  {
	    	  console.log('Ok: ' + JSON.stringify(data));
	          callback(data);
	      }, this)).error(_.bind(function(data){
	          console.log('Error: ' + JSON.stringify(data));
	          callbackError(data);
	      }));
	},
	updateVideoDTO:function(id, name, description, url,callback, callbackError){
		$.ajax({
		         url: video_service_url , 
		         type: 'POST',
		         data: {id: id,'name': name, 'description': description,'url':url}
		      }).done(_.bind(function(data)
			  {
		    	  console.log('Ok: ' + JSON.stringify(data));
		          callback(data);
		      }, this)).error(_.bind(function(data){
		          console.log('Error: ' + JSON.stringify(data));
		          callbackError(data);
		      }));
	}
};