ListaView = Backbone.View.extend({
	
	initialize: function() 
	{
		//var vs = new Views.VideosApp();
		//vs.setElement($('#container')).render();
		
	}
});


$(document).ready(function() 
{	
	console.log(vs);
	console.log(new Views);
	var vs = new Views.VideosApp();
	console.log(vs);
	vs.setElement($('#container')).render();
	console.log(vs);
});

var Models = {
	Video: Backbone.Model.extend(),
};

var Collections = {
	Videos: Backbone.Collection.extend({
		model: Models.Video,
		url: 'https://dl.dropboxusercontent.com/u/5352193/videos.json',
	
		parse: function(resp) 
		{
			return resp.data.items;
		},
	}),
};


var Views = {
	
	SingleVideo: Backbone.View.extend({
		className: 'video',
		
		initialize: function() 
		{			
			this.unVideo = _.template($('#unVideo').val());
			console.log('vista de videos');
		},
		
		render: function() 
		{
			this.$el.html(this.unVideo({video:this.model.toJSON()}));
			return this;
		},
	}),
	
	
	VideosApp: Backbone.View.extend({
		
		initialize: function() 
		{
			_.bindAll(this);
			this.listaVideos = _.template($('#listaVideos').val());
			this.collection = new Collections.Videos();
			this.collection.on('reset', this.showVideos, this);
			this.performSearch();
		},
		render: function() 
		{
			this.$el.html(this.listaVideos());
			this.showVideos();
			return this;
		},
		showVideos: function() 
		{
			this.$el.find('#videolistacontenedor').empty();
			var v = null;
			this.collection.each(function(item, idx)
			 {
				v = new Views.SingleVideo({model:item});
				this.$el.find('#videolistacontenedor').append(v.render().el);
			}, this);
			return this;
		},
		
		performSearch: function(evdata) 
		{
			evdata = evdata ||{};
			this.collection.fetch({data:{q:evdata.queryString}});
		},
	}),
	
};




