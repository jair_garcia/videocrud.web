//Initialize utils
App.Utils = App.Utils || {
	
	getTemplate : function(view,id){
		var tmpl_string='';
        $.ajax({
            url: './src/views/'+view+'.html',
            method: 'GET',
            async: false,
            contentType: 'text/html',
            dataType: 'html',
            success: function(data) {
            	tmpl_string = data;
            }
        });
        var x=$('<div></div>').append(tmpl_string);
        return $(id,x).html();
        
	},

	loadTemplate: function(view){
		console.log('loadTemplate: '+view);
		$.ajax({
            url: './js/views/'+view+'.html',
            method: 'GET',
            async: false,
            contentType: 'text/html',
            dataType: 'html',
            success: function(data) {
            	$('body').append($(data));
            }
        });
	}
};