var Models = {
		
	Video: Backbone.Model.extend(),
};

var Collections = {
	Videos: Backbone.Collection.extend({
		model: Models.Video,
		url: 'https://dl.dropboxusercontent.com/u/5352193/videos.json',
	
		parse: function(resp) 
		{
			return resp.data.items;
		},
	}),
};


var Views = {
	
	SingleVideo: Backbone.View.extend({
		className: 'video',
		
		initialize: function() 
		{			
			this.unVideo = _.template($('#unVideo').val());
		},
		
		render: function() 
		{
			this.$el.html(this.unVideo({video:this.model.toJSON()}));
			return this;
		},
	}),
	
	
	VideosApp: Backbone.View.extend({
		
		initialize: function() 
		{
			//_.bindAll(this);
			this.listaVideos = _.template($('#listaVideos').val());
			this.collection = new Collections.Videos();
			this.collection.on('reset', this.showVideos, this);
			this.performSearch();
		},
		render: function() 
		{
			this.$el.html(this.listaVideos());
			this.showVideos();
			return this;
		},
		showVideos: function() 
		{
			this.$el.find('#videolistacontenedor').empty();
			var v = null;
			this.collection.each(function(item, idx)
			 {
				v = new Views.SingleVideo({model:item});
				this.$el.find('#videolistacontenedor').append(v.render().el);
			}, this);
			return this;
		},
		
		performSearch: function(evdata) 
		{
			evdata = evdata ||{};
			this.collection.fetch({data:{q:evdata.queryString}});
		},
	}),
	
};


$(document).ready(function() 
{
	App.Utils.loadTemplate('templates');
	var vs = new Views.VideosApp();
	vs.setElement($('#container')).render();

	
});


$('body').ready(function()
{

    $.getJSON('config.json', _.bind(function(data)
    {

    	App.views=new Array();
    	//App.initialize(data);

    }, this));
    
});


App = {
		initialize: function(config)
		{	    
	        this.optionsContainer = $('#opcionesContainer');
	        this.currentView = null;
	        //this.templates = $('<div></div>');
	        //this.loadTemplates();
	        this.config=config;
	        this.url = config.server;
	        this.context = config.context;

	        
	    },   
	            
	    loadTemplates: function()
	    {
	        console.log('App.loadTemplates()');
	        this.templates.load('templates.html', _.bind(function(){
	            this.initApp();
	        }, this));
	    },
	    
	    initApp: function()
	    {
	    	try{

	    		this.views ={
					listaView : new ListaView( {el: $('#listaView', this.templates)} ),
					crearView	: new CrearView( {el: $('#crearView', this.templates)} ),
					verView : new VerView( {el: $('#verView', this.templates)} ),
					editarView : new EditarView( {el: $('#editarView', this.templates)} )
				};
				
		        this.setView("start");
		        
	    	}catch(err){
	    		alert( "error en la configuracion del producto " + err );
	    	}
	    },
	            
	    setView: function(transition)
	    {
	    	var flag=false;
	    	for(var i=0;i<this.config.transitions.length;i++)
	    	{
	    		var obj=null;
	    		obj=this.config.transitions[i];
	    		if(obj.id==transition){
	    			console.log("transition "+transition);
	        		//console.log("view "+JSON.stringify(obj));
	        		var newView=this.views[obj.view];
	    	        if(this.currentView != null){
	    	            this.currentView.onRemove();
	    	            $(this.currentView.el).detach();
	    	        }
	    	        container=$(obj.region);
	    	        container.html(newView.el);
	    	       // newView.onRender();
	    	        this.currentView = newView;
	    	        flag=true;
	    		}
	    	}
	    	if(flag==false){
	    		alert("Transicion no v�lida");
	    	}
	    }
	};

	App.Delegate = App.Delegate || {};




