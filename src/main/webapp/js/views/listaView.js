var ListaView = Backbone.View.extend({
	el: "#container",
	
	initialize: function() 
	{
		//var vs = new Views.VideosApp();
		//vs.setElement($('#container')).render();
		this.render();
	},
	template: _.template("<h3>Hello <%= who %><h3>"),
    render: function(){
        this.$el.html(this.template({who: 'world!'}));
	}

});

var Models = {
	Video: Backbone.Model.extend(),
};

var Collections = {
	Videos: Backbone.Collection.extend({
		model: Models.Video,
		url: 'https://dl.dropboxusercontent.com/u/5352193/videos.json',
	
		parse: function(resp) 
		{
			console.log('b'+JSON.stringify(resp));
			return resp.data.items;
		},
	}),
};


var Views = {
	
	SingleVideo: Backbone.View.extend({
		className: 'video',
		
		initialize: function() 
		{			
			this.unVideo = _.template($('#unVideo').val());
			console.log('vista de videos');
		},
		
		render: function() 
		{
			this.$el.html(this.unVideo({video:this.model.toJSON()}));
			return this;
		},
	}),
	
	
	VideosApp: Backbone.View.extend({
		
		initialize: function() 
		{
			//_.bindAll(this);
			this.listaVideos = _.template($('#listaView').html());
			this.collection = new Collections.Videos();
			this.collection.on('reset', this.showVideos, this);
			//this.performSearch();
		},
		render: function() 
		{
			this.showVideos();
			var videoCollection = this.collection;
			
			this.$el.html(this.listaVideos({model:videoCollection}));
			
			return this;
		},
		showVideos: function() 
		{
			console.log('show video0');
			this.$el.find('#videolistacontenedor').empty();
			console.log('show video1');
			var v = null;
			this.collection.each(function(item, idx)
			 {
				v = new Views.SingleVideo({model:item});
				this.$el.find('#videolistacontenedor').append(v.render().el);
			}, this);
			return this;
		},
		
		performSearch: function(evdata) 
		{
			evdata = evdata ||{};
			this.collection.fetch({data:{q:evdata.queryString}});
			alert(JSON.stringify(this.collection));
		},
	}),
	
};




