EditarView = Backbone.View.extend({
	


	tagName: "div",

	className: "view",

	events: {
		'click #actualizarVideo' : 'actualizarVideo',
		'click #cancel' : 'cancel',
	},
	
	cancel: function()
	{
		console.log('cancelar edicion');
		alert("cancelar edicion");
		App.setView( "videoError" );
	},
	
	actualizarVideo: function()
	{
		console.log('actualizar Video');
	      
		var ids = 0;
		var evento = $('#event').val();
	    var nombre = $('#name').val();
	    var url = $('#url').val();
	  
		if(evento == '' || nombre == '' || url == '' )
		{
			console.log(' alguno de los datos falta o est� incompleto');
			alert(" alguno de los datos falta o est� incompleto!");
			return;
		}
      
		App.Delegate.VideoDelegate.edicion
		(
			
			function(data){
				console.log('Success: ' + data);
				if(data==true){
					App.setView( "videoOk" );
					alert("      el video se ha modificado correctamente!");
				} else {
					App.setView( "videoError" );
					alert("no se pudo modificar el video");
				}
			},
			function(data){
				console.log('Error: ' + JSON.stringify(data));
			}  
		);
	},  
	

	initialize: function() {
		console.log('editarView initialize()');
	},
          
	
          
	onRemove: function(){}

});

