CrearView = Backbone.View.extend({
	
	defaults: {
		event: '',
		name: '',
		url: '' },	

	// tag name
	tagName: "div",

	// class name
  	className: "view",

	// events to process
	events: {
		'click #subirVideo' : 'subirVideo',
		'click #cancel' : 'cancel'
	},
	
	cancel: function()
	{
		console.log('cancelar video');
		alert("cancelar video");
		App.setView( "videoError" );
	},
  
	subirVideo: function()
	{
		console.log('subir Video');
      
		var evento = $('#event').val();
	    var nombre = $('#name').val();
	    var url = $('#url').val();
	  
		if(evento == '' || nombre == '' || url == '' )
		{
			console.log(' alguno de los datos falta o est� incompleto');
			alert(" alguno de los datos falta o est� incompleto!");
			return;
		}
		App.Delegate.VideoDelegate.video
		(
			evento, nombre, url,
			function(data){
				console.log('Success: ' + data);
				if(data==true){
					App.setView( "videoOk" );
					alert("                          Video registrado correctamente!");
				} else {
					App.setView( "videoError" );
					alert("cancelar video");
				}
			},
			function(data){
				console.log('Error: ' + JSON.stringify(data));
			}  
		);
	},

	initialize: function() {
		console.log('crearView initialize()');
	},
          
          
	onRemove: function(){
    },
	

});


/*var crear = new CrearView({ event:"concierto1", name: "Apertura", url: 'http://www.youtube.com/'});
var event = crear.get("event"); // "concierto1"
var name = crear.get("name"); // "Apertura"
var url = crear.get("url"); // 'http://www.youtube.com/'*/


