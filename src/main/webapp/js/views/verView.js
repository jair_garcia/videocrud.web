VerView = Backbone.View.extend({

	tagName: "div",

	className: "view",

	events: {
		'click #editarVideo' : 'editarVideo',
		'click #cancel' : 'cancel',
		'click #eliminarVideo' : 'eliminarVideo'
	},
  
	cancel: function()
	{
		console.log('cancelar video');
		App.setView( "videoError" );
	},
	
	editarVideo: function()
	{
		console.log('editar el video');
		App.setView( "modificarVideo" );
	},
	
	eliminarVideo: function()
	{
		console.log('eliminar Video');
			      
		App.Delegate.VideoDelegate.eliminar
		(
			evento, nombre, url,
			function(data){
				console.log('Success: ' + data);
				if(data==true){
					App.setView( "videoOk" );
					alert(" video eliminado!");
				} else {
					App.setView( "videoError" );
					alert("no se pudo eliminar el video");
				}
			},
			function(data){
				console.log('Error: ' + JSON.stringify(data));
			}  
		);
	},

	initialize: function() {
		console.log('verView initialize()');
	},
          
	
          
	onRemove: function(){}

});

